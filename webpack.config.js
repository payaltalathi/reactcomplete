const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');

const HTMLWebpackPluginConfig = new HTMLWebpackPlugin({
    template: path.join(__dirname, './code/src/index.html'),
    filename: 'index.html',
    inject: 'body',
  });

module.exports = {
    context: __dirname,
    entry: ['./code/src/App.jsx'],
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'bundle.js',
        publicPath: '/'
        },
    
 devServer: {
     host: 'localhost',
     port: '3000',
     publicPath: '/',
     historyApiFallback: true
         }, 
                            
    
    resolve: {
        extensions: ['.js', '.jsx', '.json'],                  
    } ,  
    stats: {
    colors: true,
    reasons: true,
    chunks: false
  },        
    module: {
        rules: [ {
            enforce: 'pre',
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'eslint-loader',
            options: {
                            presets: ['@babel/preset-env',  '@babel/preset-react']
                           }
        },
        {
            test: /\.jsx?$/,
            loader: 'babel-loader',
        }
    ]
     },
     plugins:[
         HTMLWebpackPluginConfig
         
       ]
      
 }

